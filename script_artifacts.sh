#!/bin/bash

JOB_ID=$(curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/21735184/jobs" | jq '.[1] | select(.name == "build_prod") .id')
echo $JOB_ID
curl --output artifacts.zip --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/21735184/jobs/$JOB_ID/artifacts"
